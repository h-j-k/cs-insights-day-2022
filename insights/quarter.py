def to_quarter(month: int):
    if 1 <= month <= 12:
        return f'Q{1 + (month - 1) // 3}'
    else:
        raise Exception
