from __future__ import annotations

import sys
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
from heapq import heapify, heappop, heappush


@dataclass(frozen=True)
class Point:
    x: int
    y: int


class Direction(Enum):
    Up = 'Up'
    Down = 'Down'
    Left = 'Left'
    Right = 'Right'


class Step(Enum):
    Forward = 'Forward'
    Backward = 'Backward'
    Rotate_Left = 'Rotate Left'
    Rotate_Right = 'Rotate Right'


class Heading(Enum):
    North = 'North'
    South = 'South'
    East = 'East'
    West = 'West'

    def offset(self, direction: Direction):
        match (self, direction):
            case (Heading.North, Direction.Up) | (Heading.South, Direction.Down) \
                    | (Heading.East, Direction.Left) | (Heading.West, Direction.Right):
                return lambda p: Point(x=p.x, y=p.y - 1)
            case (Heading.North, Direction.Down) | (Heading.South, Direction.Up) \
                    | (Heading.East, Direction.Right) | (Heading.West, Direction.Left):
                return lambda p: Point(x=p.x, y=p.y + 1)
            case (Heading.North, Direction.Left) | (Heading.South, Direction.Right) \
                    | (Heading.East, Direction.Down) | (Heading.West, Direction.Up):
                return lambda p: Point(x=p.x - 1, y=p.y)
            case (Heading.North, Direction.Right) | (Heading.South, Direction.Left) \
                    | (Heading.East, Direction.Up) | (Heading.West, Direction.Down):
                return lambda p: Point(x=p.x + 1, y=p.y)

    def rotate(self, step: Step) -> Heading:
        match (self, step):
            case (Heading.North, Step.Rotate_Left) | (Heading.South, Step.Rotate_Right):
                return Heading.West
            case (Heading.West, Step.Rotate_Left) | (Heading.East, Step.Rotate_Right):
                return Heading.South
            case (Heading.South, Step.Rotate_Left) | (Heading.North, Step.Rotate_Right):
                return Heading.East
            case (Heading.East, Step.Rotate_Left) | (Heading.West, Step.Rotate_Right):
                return Heading.North
            case _:
                raise Exception

    def __add__(self, direction: Direction) -> (Heading, list[Step]):
        match direction:
            case Direction.Up:
                return self, [Step.Forward]
            case Direction.Down:
                return self, [Step.Backward]
            case Direction.Left:
                return self.rotate(Step.Rotate_Left), [Step.Rotate_Left, Step.Forward]
            case Direction.Right:
                return self.rotate(Step.Rotate_Right), [Step.Rotate_Right, Step.Forward]


@dataclass
class Solution:
    heading: Heading
    position: Point
    steps: list[Step]

    def __add__(self, direction: Direction) -> Solution:
        next_heading, next_steps = self.heading + direction
        return Solution(
            heading=next_heading,
            position=(self.heading.offset(direction))(self.position),
            steps=self.steps + next_steps
        )

    def __lt__(self, other: Solution) -> bool:
        return self.cost < other.cost

    @property
    def cost(self) -> int:
        return len(self.steps)


@dataclass
class IRobot:
    targets: dict[Point, str]
    heading: Heading
    position: Point

    def __rshift__(self, target: str) -> list[Step]:
        if len(self.targets) == 0 or target not in self.targets.values():
            raise Exception
        seen = defaultdict(int)
        heap_queue = [Solution(heading=self.heading, position=self.position, steps=[])]
        heapify(heap_queue)
        while len(heap_queue) > 0:
            current = heappop(heap_queue)
            if self.targets.get(current.position) == target:
                return current.steps
            for next_solution in [current + direction for direction in Direction]:
                next_position = next_solution.position
                if (self.targets.get(next_position) == target or next_position not in self.targets) \
                        and next_solution.cost < seen.get(next_position, sys.maxsize):
                    heappush(heap_queue, next_solution)
                    seen[next_position] = next_solution.cost
        raise Exception

    def navigate(self, steps: list[Step]):
        for step in steps:
            match step:
                case Step.Forward | Step.Backward:
                    direction = (Direction.Up if step == Step.Forward else Direction.Down)
                    self.position = (self.heading.offset(direction))(self.position)
                case Step.Rotate_Left | Step.Rotate_Right:
                    self.heading = self.heading.rotate(step)
