from unittest import TestCase

from insights.irobot import Heading, IRobot, Point, Step


class Test(TestCase):
    def test_move_forward(self):
        for example in [
            (Heading.North, Point(0, 1), Point(0, 0)),
            (Heading.South, Point(0, 1), Point(0, 2)),
            (Heading.East, Point(1, 0), Point(2, 0)),
            (Heading.West, Point(1, 0), Point(0, 0)),
        ]:
            heading, position, expected = example
            with self.subTest(f'Stepping Forward at {position} {heading} = {expected}'):
                robot = IRobot(targets={}, heading=heading, position=position)
                robot.navigate([Step.Forward])
                self.assertEqual(heading, robot.heading)
                self.assertEqual(expected, robot.position)

    def test_move_backward(self):
        for example in [
            (Heading.North, Point(0, 1), Point(0, 2)),
            (Heading.South, Point(0, 1), Point(0, 0)),
            (Heading.East, Point(1, 0), Point(0, 0)),
            (Heading.West, Point(1, 0), Point(2, 0)),
        ]:
            heading, position, expected = example
            with self.subTest(f'Stepping Backward at {position} {heading} = {expected}'):
                robot = IRobot(targets={}, heading=heading, position=position)
                robot.navigate([Step.Backward])
                self.assertEqual(heading, robot.heading)
                self.assertEqual(expected, robot.position)

    def test_rotate_left(self):
        for start, expected in {
            Heading.North: Heading.West,
            Heading.South: Heading.East,
            Heading.East: Heading.North,
            Heading.West: Heading.South,
        }.items():
            with self.subTest(f'Rotate Left with {start} = {expected}'):
                robot = IRobot(targets={}, heading=start, position=Point(0, 0))
                robot.navigate([Step.Rotate_Left])
                self.assertEqual(expected, robot.heading)
                self.assertEqual(Point(0, 0), robot.position)

    def test_rotate_right(self):
        for start, expected in {
            Heading.North: Heading.East,
            Heading.South: Heading.West,
            Heading.East: Heading.South,
            Heading.West: Heading.North,
        }.items():
            with self.subTest(f'Rotate Right with {start} = {expected}'):
                robot = IRobot(targets={}, heading=start, position=Point(0, 0))
                robot.navigate([Step.Rotate_Right])
                self.assertEqual(expected, robot.heading)
                self.assertEqual(Point(0, 0), robot.position)

    def test_can_go_forward(self):
        robot = IRobot(targets={Point(0, 0): '*', Point(1, 0): 'a'}, heading=Heading.North, position=Point(0, 1))
        self.assertEqual([
            Step.Forward
        ], robot >> '*')

    def test_can_go_backward(self):
        robot = IRobot(targets={Point(0, 0): '*', Point(1, 0): 'a'}, heading=Heading.East, position=Point(1, 0))
        self.assertEqual([
            Step.Backward
        ], robot >> '*')

    def test_can_go_left(self):
        robot = IRobot(targets={Point(0, 0): '*', Point(1, 0): 'a'}, heading=Heading.North, position=Point(1, 0))
        self.assertEqual([
            Step.Rotate_Left, Step.Forward
        ], robot >> '*')

    def test_can_go_right(self):
        robot = IRobot(targets={Point(0, 0): '*', Point(1, 0): 'a'}, heading=Heading.North, position=Point(0, 0))
        self.assertEqual([
            Step.Rotate_Right, Step.Forward
        ], robot >> 'a')

    def test_can_navigate(self):
        robot = IRobot(targets={Point(0, 0): '*', Point(2, 0): 'a'}, heading=Heading.East, position=Point(0, 0))
        robot.navigate(robot >> 'a')
        self.assertEqual(Heading.East, robot.heading)
        self.assertEqual(Point(2, 0), robot.position)

    def test_unreachable(self):
        targets = {
            Point(0, 0): 'a',
            Point(2, 1): '#',
            Point(1, 2): '#',
            Point(3, 2): '#',
            Point(2, 3): '#',
        }
        robot = IRobot(targets=targets, heading=Heading.North, position=Point(2, 2))
        self.assertRaises(Exception, robot.__rshift__, 'a')

    def test_simple_pathfinding(self):
        robot = IRobot(targets={Point(10, 9): 'a'}, heading=Heading.East, position=Point(0, 0))
        self.assertEqual(
            ([Step.Forward] * 10) + [Step.Rotate_Right] + ([Step.Forward] * 9),
            robot >> 'a'
        )

    def test_invalid_pathfinding_cases(self):
        for invalid_case in [{}, {Point(0, 0): '-'}]:
            with self.subTest(f'Targets {invalid_case} will not have pathfinding solution for going to \'a\''):
                robot = IRobot(targets=invalid_case, heading=Heading.North, position=Point(0, 0))
                self.assertRaises(Exception, robot.__rshift__, 'a')
